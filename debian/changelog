luajit2 (2.1-20230119-1+apertis2) apertis; urgency=medium

  * Bump changelog to trigger license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 27 Jul 2023 14:15:37 +0530

luajit2 (2.1-20230119-1+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 13 Apr 2023 19:36:05 +0530

luajit2 (2.1-20230119-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 12 Apr 2023 20:21:34 +0530

luajit2 (2.1-20230119-1) unstable; urgency=medium

  * New upstream version 2.1-20230119 (Closes: #1029433)

 -- Mo Zhou <lumin@debian.org>  Wed, 01 Feb 2023 13:39:03 -0500

luajit2 (2.1-20220915-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Use canonical URL in Vcs-Browser.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 09 Dec 2022 10:05:14 +0000

luajit2 (2.1-20220915-1) unstable; urgency=medium

  * New upstream version 2.1-20220915
  * Refresh existing patches.
  * Remove unnecessary shlibs:Depends from -common/-dev packages.

 -- Mo Zhou <lumin@debian.org>  Sat, 29 Oct 2022 11:41:29 -0400

luajit2 (2.1-20220411-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add powerpc to Architecture (Closes: #1020508)

 -- Christian Marillat <marillat@debian.org>  Mon, 03 Oct 2022 10:33:21 +0200

luajit2 (2.1-20220411-5.1) unstable; urgency=medium

  * Non-maintainer upload

  [ Mo Zhou ]
  * No longer build on ppc64el.

 -- Paul Gevers <elbrus@debian.org>  Sat, 03 Sep 2022 13:17:37 +0200

luajit2 (2.1-20220411-5) unstable; urgency=medium

  * No Conflicts+Replaces against src:luajit on IBM Architectures.
    It yields transitional dummy packages on these archs, which depends
    on the real binary packages provided by src:luajit2.

 -- Mo Zhou <lumin@debian.org>  Wed, 08 Jun 2022 06:57:03 -0700

luajit2 (2.1-20220411-4) unstable; urgency=medium

  * Update dependency template as libluajit2-5.1-2 | libluajit-5.1-2.
    This is an advanced feature, see deb-symbols(5). Packages built against
    src:luajit2 instead of src:luajit indicates a clear mainatiner preference.
    And hence the dependency template has reversed order compared to that
    of src:luajit, i.e., libluajit-5.1-2 | libluajit2-5.1-2.

 -- Mo Zhou <lumin@debian.org>  Mon, 06 Jun 2022 19:56:18 -0700

luajit2 (2.1-20220411-3) unstable; urgency=medium

  * Fixup missing autopkgtest dependency.

 -- Mo Zhou <lumin@debian.org>  Sat, 04 Jun 2022 22:19:48 -0700

luajit2 (2.1-20220411-2) unstable; urgency=medium

  * Allow using libluajit-5.1-2 as dependency fallback.
    This is a dependency-template feature, see deb-symbols(5) for details.
  * Override lintian warnings about the binary package name.

 -- Mo Zhou <lumin@debian.org>  Sat, 04 Jun 2022 22:12:04 -0700

luajit2 (2.1-20220411-1) unstable; urgency=medium

  * Refresh symbols again for the rest architectures.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Thu, 02 Jun 2022 08:27:04 -0700

luajit2 (2.1-20220411-1~exp2) experimental; urgency=medium

  [ Frédéric Bonnard ]
  * Improve symbols file

 -- Mo Zhou <lumin@debian.org>  Wed, 01 Jun 2022 20:40:09 -0700

luajit2 (2.1-20220411-1~exp1) experimental; urgency=medium

  * New upstream version 2.1-20220411. (Closes: #1011320)
  * Borrowed bits from Debian's src:luajit packaging.
  * This package provides the same contents as src:luajit.

 -- Mo Zhou <lumin@debian.org>  Fri, 20 May 2022 10:24:40 -0400
